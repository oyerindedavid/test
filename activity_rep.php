<?php include('header.php');?>

<?php
 if(isset($_POST['submit'])){
	 $date = mysqli_real_escape_string($con, $_POST['date']);
	 $day = mysqli_real_escape_string($con, $_POST['day']);
	 $programme = mysqli_real_escape_string($con, $_POST['programme']);
	 $venue = mysqli_real_escape_string($con, $_POST['venue']);
	 $time = mysqli_real_escape_string($con, $_POST['time']);
	 $male = mysqli_real_escape_string($con, $_POST['male']);
	 $female = mysqli_real_escape_string($con, $_POST['female']);
	 $ssattendance = mysqli_real_escape_string($con, $_POST['ssattendance']);
	 $preacher = mysqli_real_escape_string($con, $_POST['preacher']);
	 $total = $male + $female;
	 $user_id = $_SESSION['user'];
	 $month = mysqli_real_escape_string($con, $_POST['month']);
	 $year = mysqli_real_escape_string($con, $_POST['year']);
	 
	 $sql =  "INSERT INTO activity_report (date, day, programme, venue, time,  male, female, total, ssattendance, preacher, year, month, user_id)
           	  VALUES 
			 ('$date', '$day', '$programme', '$venue', '$time', '$male',  '$female', '$total', '$ssattendance', '$preacher', '$year', '$month', '$user_id')";
	 $res = 	mysqli_query($con, $sql);
	 
	 if (!$res){
		 $output = "Failed to add activity";
	   }
	   else{
		 $output = "Activity added successfully";
	   }
 }
?>

        <!-- page content update for githup-->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <marquee style="color:red"><?php echo $resAlert['message'] ?></marquee>
            </div><br />
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <a href="activity_rep.php">Activity | <a href="fin_report.php"> Financial Report |</a> <a href="further_report.php"> Further Report |</a> <a href="suggestion_form.php"> Suggestion</a> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
				  <p class="text-muted font-13 m-b-30">
                      <strong>Activity Form.</strong> 
                    </p>
				    
				  
				   
                    <form  method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">
					
					<?php
			              if (isset($output) ) {
				       ?>
				   <div class="form-group">
            	   <div class="alert alert-danger control-label col-md-12 col-sm-12 col-xs-12">
				   <span class="glyphicon glyphicon-info-sign"></span> <?php echo $output; ?>
                </div>
            	</div>
                <?php
			    }
			    ?>
				
				       <label class="control-label col-md-1 col-sm-1 col-xs-12" for="first-name">Year <span class="required">*</span>
                        </label>
                     <div class="col-md-2 col-sm-2 col-xs-12">
						
                       <select class="form-control col-md-7 col-xs-12" name="year" required>
						<option></option>
						<option>2017</option>
						<option>2018</option>
						<option>2019</option>
						<option>2020</option>
						<option>2021</option>
						<option>2022</option>
						<option>2021</option>
						<option>2023</option>
						<option>2024</option>
						<option>2025</option>
						<option>2026</option>
						<option>2027</option>
						<option>2028</option>
						<option>2029</option>
						<option>2030</option>
					  </select>
                     </div>
						
						<label class="control-label col-md-1 col-sm-1 col-xs-12" for="first-name">Month <span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <select class="form-control col-md-7 col-xs-12" name="month" required>
						<option></option>
						<option>January</option>
						<option>February</option>
						<option>March</option>
						<option>April</option>
						<option>May</option>
						<option>June</option>
						<option>July</option>
						<option>August</option>
						<option>September</option>
						<option>October</option>
						<option>November</option>
						<option>December</option>
						</select>
                        </div>
						<br />
						<br />
						<br />

                      <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                        <input type="date" class="form-control has-feedback-left" name="date"  placeholder="" required>
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="form-group">
                       <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                          <select class="form-control" name="day" required>
                            <option>Sunday Morning</option>
							<option>Sunday Evening</option>
                            <option>Monday</option>
                            <option>Tuesday</option>
                            <option>Wednesaday</option>
                            <option>Thursday</option>
                            <option>Friday</option>
                            <option>Saturday</option>
						</select>
                        </div>
                      </div>
					  
					  <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" name="programme" placeholder="Programme" required>
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" name="venue" placeholder="Venue">
                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                      </div>
					  
					  <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" name="time" placeholder="Time" required>
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
					  
					  <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" name="male" placeholder="Male">
                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                      </div>
					  
					  <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" name="female" placeholder="Female">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
					  <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" name="ssattendance" placeholder="Sunday School Attendance">
                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" name="preacher"  placeholder="Preacher">
                        <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-offset-10">
                          <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

            

           

            


            
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <!-- <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div> -->
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/moment/min/moment.min.js"></script>
    <script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="vendors/starrr/dist/starrr.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>
	
  </body>
</html>
