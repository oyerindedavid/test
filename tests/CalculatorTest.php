<?php



use PHPUnit\Framework\TestCase;
use MyApp\Math\Calculator;

require_once realpath("vendor/autoload.php");

class CalculatorTest extends TestCase{

    public function testAdd(){

        $calculator = new Calculator;

        $result = $calculator::add(20, 5);

        $this->assertEquals(25, $result);
    }

    public function testSubtract(){

        $calculator = new Calculator;

        $result = $calculator::subtract(25, 5);

        $this->assertEquals(20, $result);
    }

    public function testMultiply(){

        $calculator = new Calculator;

        $result = $calculator::multiply(25, 5);

        $this->assertEquals(125, $result);
    }
}