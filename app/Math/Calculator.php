<?php

namespace MyApp\Math;

class Calculator{

    public static function add($a, $b){
       return $a + $b;
    }

    public static function subtract($a, $b){
        return $a - $b;
     }

     public static function multiply($a, $b){
        return $a * $b;
     }

}


