<?php include("connectdb.php"); ?>
<?php include("side_nav.php"); ?>
<?php include("top_nav.php"); ?>
<?php

if(isset($_POST['submit'])){
	
  $category = mysqli_real_escape_string($con, $_POST['category']);
  $query = mysqli_query($con, "SELECT * FROM category WHERE category_name = '$category'");
  $count = mysqli_num_rows($query);
  if($count == 0){
  
      $sql = mysqli_query($con, "INSERT INTO category (category_name, user_id) VALUE ('$category', $user_id)");
      $output = "category added successfully.";
  }else{
	  $output = "category already added.";
  }
  
}

?>

   

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>New Category</h3>
              </div>

             </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add Category <small>New category</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off" class="form-horizontal form-label-left">

					<?php
			             if ( isset($output) ) {
				      ?>
				         <div class="form-group">
            	            <div class="alert alert-danger">
				               <span class="glyphicon glyphicon-info-sign"></span> <?php echo $output; ?>
                            </div>
            	         </div>
                     <?php
			           }
			         ?>
					
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="category" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter category to add">
                        </div>
                      </div>
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

  <?php include("footer.php"); ?>         
		